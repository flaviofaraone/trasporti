<x-layout>

    <section class="page-section" id="contact">
        <div class="container-fluid my-4 py-4">
            <div class="row">
                <div class="col">
                    <div class="text-center align-bottom">
                        <h2 class="section-heading text-uppercase">grazie per per averci contattato.</h2>
                        <h3 class="section-subheading text-light">ti risponderemo al più presto.</h3>

                        <div class="row d-flex justify-content-around">

                            <button class="btn btn-primary btn-xl text-uppercase  m-4 p-4" id="sendMessageButton" type="submit"><a href="/" class="text-dark">torna indietro</a></button>

                            <img src="img/mercedes.png" width="200px" alt="">
                            {{-- <button class="btn btn-primary btn-xl text-uppercase  m-4 p-4" id="sendMessageButton" type="submit"><a href="https://www.google.com/" class="text-dark">torna su google</a></button> --}}

                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </section>
    
</x-layout>