<footer class="footer py-4">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4 text-lg-left">Copyright Ferrante Autotrasporti © 2020 <br><p>P.Iva:1234567890</p></div>
            <div class="col-lg-4 my-3 my-lg-0">
                {{-- <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-facebook-f"></i></a> --}}
                <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-linkedin-in"></i></a>
            </div>
            <div class="col-lg-4 text-lg-right ">
                {{-- <a class="mr-3 text-success" href="tel:3896543417">3896543417</a> --}}
                <p>powered by <a href="http://www.espoint.it/">espoint.it</a></p>
                {{-- <a class="mr-3 text-success" href="#!">Privacy Policy</a>
                <a class="text-success" href="#!">Terms of Use</a> --}}
            </div>
        </div>
    </div>
</footer>