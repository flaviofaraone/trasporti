<x-layout>
    <!-- Navigation-->
    
    <!-- Masthead-->
    <header class="masthead">
        <div class="container">
            {{-- <div class="masthead-heading text-uppercase text-success">ferrante<br>trasporti</div> --}}
            <div class="masthead-heading text-uppercase text-light"><span class="text-success">ferrante<br>trasporti<br></span> e logistica  per <br> il tuo business</div>
            <a class="btn btn-success btn-xl text-uppercase js-scroll-trigge text-light" href="#contact">contattaci</a>
        </div>
    </header>
    <!-- Services-->
    <section class="page-section" id="services">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Servizi</h2>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <span class="fa-stack fa-4x">
                        <i class="fas fa-circle fa-stack-2x text-success"></i>
                        <i class="fas fa-truck-moving fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="my-3">trasporti nazionali e internazionali</h4>
                    <p class="text-muted">linee giornaliere in sicilia e spagna <br>trasporti in tutta italia ed estero. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Architecto hic dolor itaque tenetur minima! Quod ipsum quasi eum laboriosam, aliquid maxime, non eligendi quis animi quidem labore laudantium. Esse, ullam!</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fas fa-circle fa-stack-2x text-success"></i>
                        <i class="fas fa-map-marker-alt fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="my-3">contatti</h4>
                    <p class="text-muted">via modugno,12 -70126 BARI ( BA ) <br> uffici presso interporto di bari <br>
                    <a class="mr-3 text-success" href="tel:3896543417">3896543417</a> <br>
                    <a class="mr-3 text-success" href="tel:080080080">080080080</a> <br>
                    <a class="emailTag text-success" href="mailto:ferrante@mail.com">ferrante@mail.it</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
     <!-- Contact-->
    <section class="page-section" id="contact">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Contattaci</h2>
                <h3 class="section-subheading text-light">per conoscere il servizio più adatto alle tue necessità.</h3>
            </div>
            
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            
            <form id="contactForm" name="sentMessage" novalidate="novalidate" method="POST" action="{{ route('submit') }}">
                @csrf
                <div class="row align-items-stretch mb-5">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input class="form-control" id="name" value="{{old('name')}}" name="name" type="text" placeholder="Nome *" required="required"/>
                            @error('name')
                            <div class="alert alert-danger">campo assente o troppo lungo | {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input class="form-control" id="email" value="{{old('email')}}" name="email" type="email" placeholder="Email *" required="required"/>
                            @error('email')
                            <div class="alert alert-danger">campo assente o troppo lungo | {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group mb-md-0">
                            <input class="form-control" id="phone" value="{{old('phone')}}" name="phone" type="tel" placeholder="Telefono *" required="required"/>
                            @error('phone')
                            <div class="alert alert-danger">campo assente o troppo lungo | {{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-textarea mb-md-0">
                            <textarea class="form-control" id="description" name="description" placeholder="Descrizione *" required="required">{{old('description')}}</textarea>
                            @error('description')
                            <div class="alert alert-danger">descrizione assente o troppo lunga | {{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <div id="success"></div>
                    <button class="btn btn-success btn-xl text-uppercase" id="sendMessageButton" type="submit">contattaci</button>
                </div>
            </form>
        </div>
    </section>
    <!-- Footer-->

    </x-layout>