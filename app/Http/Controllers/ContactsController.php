<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactReceived;
use App\Http\Requests\Contact;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactsController extends Controller
{
    public function submit(Contact $request){

    
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $description = $request->input('description');

        $emailAdmin='flaviofaraon@hotmail.it';
        $bag =compact('name','email','phone','description');
        $contactMail =new ContactReceived ($bag);

    
      Mail::to($emailAdmin)->send($contactMail);

      return redirect (route('thankyou'));

    }


    public function thankyou(){

      return view('contacts.thankyou');
    }
}
