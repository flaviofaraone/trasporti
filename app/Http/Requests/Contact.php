<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Contracts\Service\Attribute\Required;

class Contact extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'Required|min:3|max:100',
            'email'=>'Required|min:3|max:80',
            'phone'=>'Required|min:3|max:20',
            'description'=>'Required|min:3|max:5000',
        ];
    }

    public function message(){

        [
            'name.required'=>'inserisci il tuo nome',
            'email.required'=>'inserisci la tua email ',
            'phone.required'=>'inserisci il tuo  numero',
            'description.required'=>'inserisci la descrizione',

            'name.min'=>'inserisci il tuo nome',
            'email.min'=>'inserisci la tua email ',
            'phone.min'=>'inserisci il tuo  numero',
            'description.min'=>'inserisci la descrizione',

            'name.max'=>'nome troppo lungo',
            'email.max'=>'indirizzo email troppo lungo',
            'phone.max'=>'numero troppo lungo',
            'description.max'=>'descrizione troppo lunga , massimo 5000 caratteri',


        ];
    }

        //sovrascrive il sistema standard laravel e indirizza al punto #contact
        protected function getRedirectUrl()
        {
            $url = $this->redirector->getUrlGenerator();
            return $url->previous() . '#contact';
        }

}
